<?php
$config = [
	'host' => 'localhost',
  'user' => 'root',
	'password' => '',
	'database' => 'developertest',
];
$sql = 'SELECT * FROM `testtable`';
$db = new DataBase($config);
$db->query($sql);


class DataBase
{
    private $config;
    private $pdo;

    public function __construct($config = NULL) {
			if($config){
				try {
					$connect = "mysql:host=" . $config['host'] . ";dbname=" . $config['database'] . ";charset=utf8";
					$this->pdo = new PDO($connect, $config['user'] , $config['password']);
					$this->pdo->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC ); 
        	$this->pdo->setAttribute( PDO::ATTR_PERSISTENT, true );
				} 
				
				catch(PDOException $ex) {
						echo "Ошибка подключения!";
				}
				
			}

    }

    public function query($sql) {
			$this->pdo->query($sql);
			foreach ($this->pdo->query($sql) as $row) {
        print $row['name'];
    	}
    }

    private function connect() {

    }
}
?>